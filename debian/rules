#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

ifneq (,$(filter noopt,$(DEB_BUILD_OPTIONS)))
	CMAKE_BUILD_TYPE=Debug
else
	CMAKE_BUILD_TYPE=Release
endif

override_dh_auto_configure:
	dh_auto_configure -- \
		-DCMAKE_BUILD_TYPE=$(CMAKE_BUILD_TYPE) \
		-DCMAKE_BUILD_STRIP=FALSE \
		-DXDG_STD=TRUE \
		-DREBUILD_SHADER=TRUE \
		-DBUILD_REPLAY_LOADERS=FALSE \
		-DDISABLE_ADVANCE_SIMD=TRUE \
		-DDISABLE_BUILD_DATE=TRUE \
		-DDISABLE_CHEATS_ZIP=FALSE \
		-DDISABLE_DEV9GHZDRK=TRUE \
		-DDISABLE_PCSX2_WRAPPER=TRUE \
		-DBIN_DIR="/usr/games" \
		-DDOC_DIR="/usr/share/doc/pcsx2" \
		-DGAMEINDEX_DIR="/usr/share/games/pcsx2" \
		-DPLUGIN_DIR="/usr/lib/$(DEB_HOST_MULTIARCH)/pcsx2" \
		-DGTK3_API=TRUE \
		-DPACKAGE_MODE=TRUE

override_dh_auto_install:
	dh_auto_install --destdir="$(CURDIR)/debian/pcsx2"

override_dh_compress:
	dh_compress -X.pdf

override_dh_installdocs:
	dh_installdocs --link-doc=pcsx2

# Silence lintian warnings about *-has-useless-call-to-ldconfig
override_dh_makeshlibs:

override_dh_shlibdeps:
	# When doing dpkg-buildpackage -ai386 don't depend on lib32stdc++6,
	# lib32gcc1, and libc6-i386 since they don't exist in i386. Correctly pick
	# the multiarch variant instead of the multilib variant.
	dh_shlibdeps -l/lib/$(DEB_HOST_MULTIARCH)/:/usr/lib/$(DEB_HOST_MULTIARCH)/

%:
	dh $@

get-orig-source:
	sh debian/get-git-source.sh

.PHONY: get-orig-source
